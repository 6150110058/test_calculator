import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class Calculator extends StatefulWidget {
  const Calculator({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  String result = "0";
  String expression = "";

  buttonPressed(String value) {
    //อัพเดทค่า result
    setState(() {
      if (value == "CLEAR") {
        result = "0";
      } else if (value == ".") {
        if (result.contains(".")) {
          return;
        } else {
          result = result + value;
        }
      } //การคำนวน
      else if (value == "=") {
        expression = result.replaceAll("x", "*");
        Parser p = Parser(); //ได้มาจาก math_expressions
        Expression exp = p.parse(expression);
        // เปลี่ยนให้เปนตัวเลข
        ContextModel cm = ContextModel();
        dynamic Calculate = exp.evaluate(EvaluationType.REAL, cm);
        result = "$Calculate"; //ทำให้กลับเป็นตัวอักษร
      } else {
        if (result == "0") {
          result = value;
        } else {
          //ทำให้สามารถคำนวนหลายค่าได้
          result = result + value;
        }
      }
    });
  }

  Widget myButton(String buttonNum, Color buttonColor) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: buttonColor,
        ),
        height: 70,
        margin: EdgeInsets.all(2),
        child: Expanded(
          child: InkWell(
            onTap: () {
              buttonPressed(buttonNum);
            },
            child: Center(
              child: Text(
                buttonNum,
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
              alignment: Alignment.centerRight,
              child: Text(
                result,
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              child: Divider(color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Column(
                children: [
                  Row(
                    children: [
                      myButton("7", Colors.black12),
                      myButton("8", Colors.black12),
                      myButton("9", Colors.black12),
                      myButton("/", Colors.blue.shade300),
                    ],
                  ),
                  Row(
                    children: [
                      myButton("4", Colors.black12),
                      myButton("5", Colors.black12),
                      myButton("6", Colors.black12),
                      myButton("x", Colors.blue.shade300),
                    ],
                  ),
                  Row(
                    children: [
                      myButton("1", Colors.black12),
                      myButton("2", Colors.black12),
                      myButton("3", Colors.black12),
                      myButton("-", Colors.blue.shade300),
                    ],
                  ),
                  Row(
                    children: [
                      myButton(".", Colors.black12),
                      myButton("0", Colors.black12),
                      myButton("00", Colors.black12),
                      myButton("+", Colors.blue.shade300),
                    ],
                  ),
                  Row(
                    children: [
                      myButton("CLEAR", Colors.red.shade400),
                      myButton("=", Colors.green.shade400),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
